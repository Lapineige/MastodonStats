# MastodonStats

Script qui extrait de l'archive des pouets de Mastodon des statistiques (ex: le nombre de pouets/privés/etc).

----

**Important**: ce script est prévu pour un usage personnel (le mien, à l'origine), il me semble que cela peut intéresser d'autres personnes (d'où ce dépôt) de connaître quelques statistiques sur *leur* compte.
S'il contient uniquement vos informations personnelles, il peut donner des informations sur vos conversations avec d'autres personnes (ce qui peut leur poser problème, notamment si ce sont des conversations privées (messages directs, etc)).
Si vous voulez publier ces données (ce qui *n'est pas* l'objet initial de ce script), considérez d'abord l'impact potentiel sur d'autres personnes (et demandez leur). Merci :)

----

Pour télécharger votre archive: `https://domainedevotreinstance.tld/settings/export`
Ensuite extrayez l'archive.

Pour utiliser cet outil:
* Modifier le fichier dans un traitement de texte, ajouter vos pseudos et adresse de l'instance dans les "" des lignes 15 et 16
* ouvrez un terminal dans le dossier contenant le script
* (le script doit être exécutable, modifiez les droits au besoin) 
* exécutez le avec `python3 mastodonStats.py /chemin/vers/votre/archive/mastodon/output.json` (peut être un chemin relatif)