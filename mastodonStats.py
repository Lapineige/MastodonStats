# Astuce pour rendre le JSON lisible: python -m json.tool outbox.json > pretty.json

import json
import sys

print("Statistiques en cours sur : ", sys.argv[1])

with open(sys.argv[1]) as json_data:
    d = json.load(json_data)

pouets = d["orderedItems"]

stats = {"directs":0, "privés":0, "nonlistés":0, "publics":0, "repouets":0, "mentions":{}, "hashtag":{} }

USER = "" # Pseudo
INSTANCE = "" # Instance
PUBLIC_STREAM = "https://www.w3.org/ns/activitystreams#Public"

for pouet in pouets:

    if pouet["to"] == []:
        print ("#MentionFail ? :) : ", pouet["id"][:-8])

    elif pouet["to"][0] == PUBLIC_STREAM:
        if pouet["type"] == "Announce":
            stats["repouets"] += 1
        else:
            stats["publics"] += 1

    elif pouet["to"][0] == "https://{0}/users/{1}/followers".format(INSTANCE, USER):
        if pouet["cc"] == []:
            stats["privés"] += 1
        else:
            if PUBLIC_STREAM in pouet["cc"][0]:
                stats["nonlistés"] += 1
            else:
                stats["privés"] += 1

    elif not "/followers" in pouet["to"][0]:
        stats["directs"] += 1

    else:
        print("y'a des oubliés: ", pouet["id"], pouet["to"])

    if "tag" in pouet["object"] and type(pouet["object"])==dict:
        for tag in pouet["object"]["tag"]:

            if tag["type"] == "Mention":
                if tag["name"] in stats["mentions"]:
                    stats["mentions"][tag["name"]] += 1
                else:
                    stats["mentions"][tag["name"]] = 1

            elif tag["type"] == "Hashtag":
                if tag["name"] in stats["hashtag"]:
                    stats["hashtag"][tag["name"]] += 1
                else:
                    stats["hashtag"][tag["name"]] = 1


nb_msg_total = stats["publics"]+ stats["nonlistés"] + stats["privés"] + stats["directs"] + stats["repouets"]
proportions_pouets = (round(stats["publics"]/nb_msg_total*100,1),
               round(stats["nonlistés"]/nb_msg_total*100,1),
               round(stats["privés"]/nb_msg_total*100,1),
               round(stats["directs"]/nb_msg_total*100,1),
               round(stats["repouets"]/nb_msg_total*100,1))

print( ("Nombre de messages publics {0} ({1}%); non listés {2} ({3}%); privés {4} ({5}%); directs {6} ({7}%); repouets {8} ({9}%)").format(
    stats["publics"],
    proportions_pouets[0],
    stats["nonlistés"],
    proportions_pouets[1],
    stats["privés"],
    proportions_pouets[2],
    stats["directs"],
    proportions_pouets[3],
    stats["repouets"],
    proportions_pouets[4]) )

print()

mentions_tri = sorted(stats["mentions"].items(), key=lambda kv: kv[1])
mentions_tri.reverse()
total_mentions = sum(stats["mentions"].values())

print("Pseudos mentionnés (par quantité décroissante):")
for mention in mentions_tri:
    print("{0} : {1} ({2}%)".format(
        mention[0],
        mention[1],
        round(mention[1]/total_mentions*100,1) ))

hashtag_tri = sorted(stats["hashtag"].items(), key=lambda kv: kv[1])
hashtag_tri.reverse()
total_hashtag = sum(stats["hashtag"].values())

print()

print("Hashtag utilisés (par quantité décroissante):")
for hashtag in hashtag_tri:
    print("{0} : {1} ({2}%)".format(
        hashtag[0],
        hashtag[1],
        round(hashtag[1]/total_hashtag*100,1) ))